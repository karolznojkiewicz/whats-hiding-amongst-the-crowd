(function () {
    const detectWord = (str) => {
        const reg = /[a-z]/g;
        const letter = str.match(reg); //[]
        return letter.join(''); //string
    };

    detectWord('UcUNFYGaFYFYGtNUH');
})();
